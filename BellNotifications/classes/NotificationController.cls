public class NotificationController {
    @AuraEnabled
    public static List<ConnectApi.FeedElement> getNotifications() {
        ConnectApi.Community objCommunity = null;
        //Community_Settings__c objConfig = null;
        List<ConnectApi.FeedElement> lstNews = null;
	ConnectApi.FeedElementPage objPage = null;
        String strSiteUrl = null;
        
        //objConfig = HelperFunctions.retrieveConfig();
        objCommunity = retrieveCommunity('PORTAL NAME');        

	objPage = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(objCommunity.Id, ConnectApi.FeedType.News, 'me');
        strSiteUrl = objCommunity.siteUrl;
        
        if(objPage != null && objPage.elements != null && objPage.elements.size() > 0) {
            lstNews = objPage.elements;
        }
              
        return lstNews;
    }  
    
    private static ConnectApi.Community retrieveCommunity(String strCommunityName) {
        ConnectApi.Community objCommunity = null;
        ConnectApi.CommunityPage objCommunities = null;
         
        if(String.isNotBlank(strCommunityName)) {
            objCommunities = ConnectApi.Communities.getCommunities();

            if(objCommunities != null && objCommunities.total > 0) {
                for(ConnectApi.Community objSearchCommunity : objCommunities.communities) {
                    if(objSearchCommunity.Name == strCommunityName) {
                        objCommunity = objSearchCommunity;
                    }
                }
            }            
        } 
        
        return objCommunity;
    }
}
