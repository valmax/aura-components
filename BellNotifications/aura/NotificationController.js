({
	retrieveUnreadNotifications : function(objComponent, objEvent, objHelper) {
        objHelper.loadNotifications(objComponent);
	},
    showNotifications : function(objComponent, objEvent, objHelper) {
        objHelper.toggleNotifications(objComponent);
    }
})