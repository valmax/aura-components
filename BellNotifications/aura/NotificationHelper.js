({
    loadNotifications : function(objComponent) {
        var objControllerAction = null;
        var strState = null;
        var strSiteUrl = null;
        
        objControllerAction = objComponent.get("c.retrieveSiteUrl");
        objControllerAction.setCallback(this, function(objResponse) {           
            strState = objResponse.getState();
            
            if (objComponent.isValid() && strState === "SUCCESS") {
                strSiteUrl = objResponse.getReturnValue();
                
                if(strSiteUrl != null && strSiteUrl.length > 0) {
                    this.retrieveNotifications(strSiteUrl, objComponent);                    
                }
            } else {
                console.log("Failed with state: " + strState);
            }
        });
        $A.enqueueAction(objControllerAction);
    },
	retrieveNotifications : function(strSiteUrl, objComponent) {
		var objControllerAction = null;
        var arrFeedElements = null;
        var intUnreadNotifications = 0;
        var strState = null;
        var objNotifications = null;
        var strHTML = null;
        
        objControllerAction = objComponent.get("c.getNotifications");
        objControllerAction.setCallback(this, function(objResponse) {           
            strState = objResponse.getState();
            
            if (objComponent.isValid() && strState === "SUCCESS") {
                arrFeedElements = objResponse.getReturnValue();
                
                if(arrFeedElements != null && arrFeedElements.length != null) {
                    intUnreadNotifications = arrFeedElements.length;
                    objComponent.set("v.unreadNotifications", intUnreadNotifications);

                    objNotifications = objComponent.find("divNotificationCount");

                    if(intUnreadNotifications > 0) {
                        $A.util.removeClass(objNotifications, "counterLabelContainerHidden");
                        $A.util.addClass(objNotifications, "counterLabelContainer");
                        
                        strHTML = '';
                        for(var intIndex = 0; intIndex < intUnreadNotifications; intIndex++) {
                            strHTML += this.createNotificationRow(strSiteUrl, arrFeedElements[intIndex]); 
                        }
                        document.getElementById('NotificationElements').innerHTML = strHTML;
                    } else {
                        $A.util.removeClass(objNotifications, "counterLabelContainer");
                        $A.util.addClass(objNotifications, "counterLabelContainerHidden");
                    }                    
                } else {
                    $A.util.removeClass(objNotifications, "counterLabelContainer");
                    $A.util.addClass(objNotifications, "counterLabelContainerHidden");                    
                }
            } else {
                console.log("Failed with state: " + strState);
            }
        });
        $A.enqueueAction(objControllerAction);
	},        
    createNotificationRow : function(strSiteUrl, objFeedElement) {
        var strHTML = null;
        
        strHTML = '<li class="notification-row notification-unread unsNotificationsListRow">';
        strHTML += '	<a class="notification-link" href="' + strSiteUrl + objFeedElement.id + '">';
        strHTML += '		<div class="notification-content">';
        strHTML += '			<div class="notification-avatar">';
        strHTML += '				<span class="uiImage">';
        strHTML += '				</span>';
        strHTML += '			</div>';
        strHTML += '			<div class="notification-content">';
        strHTML += '				<span class="notification-text-title uiOutputText" dir="ltr">';
        strHTML += '					' + objFeedElement.header.text + '';
        strHTML += '				</span>';
        if(objFeedElement.body != null && objFeedElement.body.text != null) {
            strHTML += '				<span class="notification-text uiOutputText" dir="ltr">';
            strHTML += '					' + objFeedElement.body.text + '<br />';
            strHTML += '				</span>';
        }
        strHTML += '				<span title="">';
        strHTML += '					<span class="notification-age uiOutputText" dir="ltr">';
        strHTML += ' ' + objFeedElement.relativeCreatedDate + '';
        strHTML += '					</span>';
        strHTML += '					<span class="">';
        strHTML += '						<span class="assistive-unread-mark"> ●</span>';
        strHTML += '						<span class="assistiveText">Unread</span>';
        strHTML += '					</span>';
        strHTML += '				</span>';
        strHTML += '			</div>';
        strHTML += '		</div>';
        strHTML += '	</a>';
        strHTML += '</li>';
        
        return strHTML;
    }, 
    toggleNotifications : function(objComponent) {
        var objNotification = null;
        
        objNotification = objComponent.find("divNotification");

        if(document.getElementById('divNotification').className.indexOf('close') >= 0 ) {
            $A.util.removeClass(objNotification, "close");
            $A.util.addClass(objNotification, "open");                    
        } else {
            $A.util.removeClass(objNotification, "open");
            $A.util.addClass(objNotification, "close");                            
        }
    }
})